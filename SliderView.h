//
//  SliderView.h
//  MobileID
//
//  Created by Kuba Rejnhard on 16.03.2014.
//  Copyright (c) 2014 HEMNES. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SliderView : UIView
@property (nonatomic, strong)UIButton* slideButton;
@end
