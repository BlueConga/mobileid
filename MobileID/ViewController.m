//
//  ViewController.m
//  MobileID
//
//  Created by Filip Jakubowski on 15.03.2014.
//  Copyright (c) 2014 HEMNES. All rights reserved.
//

#import "ViewController.h"
#import <Parse/Parse.h>
#import "TabView.h"
#import "SliderView.h"
@interface ViewController ()
@property (nonatomic, strong) NSMutableArray * tabArray;
@property (nonatomic, strong) UIView * backgroundView;
@property (nonatomic, strong) SliderView *sliderView;
@property (nonatomic, strong) UIButton *buttonLeft;
@property (nonatomic, strong) UIButton *buttonRight;

@end

@implementation ViewController
@synthesize tabArray, backgroundView, sliderView, buttonLeft, buttonRight;

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (![PFUser currentUser]) { // No user logged in
        // Create the log in view controller
        PFLogInViewController *logInViewController = [[PFLogInViewController alloc] init];
        [logInViewController setDelegate:self]; // Set ourselves as the delegate
        
        // Create the sign up view controller
        PFSignUpViewController *signUpViewController = [[PFSignUpViewController alloc] init];
        [signUpViewController setDelegate:self]; // Set ourselves as the delegate
        
        // Assign our sign up controller to be displayed from the login controller
        [logInViewController setSignUpController:signUpViewController];
        
        // Present the log in view controller
        [self presentViewController:logInViewController animated:YES completion:NULL];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    backgroundView = [[UIView alloc ]initWithFrame:CGRectMake(self.view.frame.origin.x-20, self.view.frame.origin.y-20, self.view.frame.size.width+40, self.view.frame.size.height+40)];
    /*
    
    UIInterpolatingMotionEffect *verticalMotionEffect =
    [[UIInterpolatingMotionEffect alloc]
     initWithKeyPath:@"center.y"
     type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
    verticalMotionEffect.minimumRelativeValue = @(20);
    verticalMotionEffect.maximumRelativeValue = @(-20);
    
    // Set horizontal effect
    UIInterpolatingMotionEffect *horizontalMotionEffect =
    [[UIInterpolatingMotionEffect alloc]
     initWithKeyPath:@"center.x"
     type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
    horizontalMotionEffect.minimumRelativeValue = @(20);
    horizontalMotionEffect.maximumRelativeValue = @(-20);
    
    // Create group to combine both
    UIMotionEffectGroup *group = [UIMotionEffectGroup new];
    group.motionEffects = @[horizontalMotionEffect, verticalMotionEffect];
    
    // Add both effects to your view
    [backgroundView addMotionEffect:group];
    
    
    UIInterpolatingMotionEffect *verticalMotionEffect2 =
    [[UIInterpolatingMotionEffect alloc]
     initWithKeyPath:@"center.y"
     type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
    verticalMotionEffect2.minimumRelativeValue = @(-10);
    verticalMotionEffect2.maximumRelativeValue = @(10);
    
    // Set horizontal effect
    UIInterpolatingMotionEffect *horizontalMotionEffect2 =
    [[UIInterpolatingMotionEffect alloc]
     initWithKeyPath:@"center.x"
     type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
    horizontalMotionEffect2.minimumRelativeValue = @(-10);
    horizontalMotionEffect2.maximumRelativeValue = @(10);
    
    // Create group to combine both
    UIMotionEffectGroup *group2 = [UIMotionEffectGroup new];
    group2.motionEffects = @[horizontalMotionEffect2, verticalMotionEffect2];
    
    
    
    */
    
    
    
    
    
    
    self.backgroundView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"backg"]];
    
    [self.view addSubview: backgroundView];
    NSArray* nameArr = [NSArray arrayWithObjects:
                        [UIColor colorWithRed: 66/255.0f green: 205/255.0f blue: 0/255.0f alpha: 1],
                        [UIColor colorWithRed: 0/255.0f green: 122/255.0f blue: 255/255.0f alpha: 1],
                        [UIColor colorWithRed: 255/255.0f green: 161/255.0f blue: 0/255.0f alpha: 1],
                        [UIColor colorWithRed: 86/255.0f green: 183/255.0f blue: 241/255.0f alpha: 1],
                        [UIColor colorWithRed: 255/255.0f green: 45/255.0f blue: 85/255.0f alpha: 1],
                        [UIColor colorWithRed: 255/255.0f green: 204/255.0f blue: 0/255.0f alpha: 1],
                        [UIColor colorWithRed: 66/255.0f green: 205/255.0f blue: 0/255.0f alpha: 1],
                        [UIColor colorWithRed: 0/255.0f green: 122/255.0f blue: 255/255.0f alpha: 1],
                        [UIColor colorWithRed: 255/255.0f green: 161/255.0f blue: 0/255.0f alpha: 1],
                        
                        
                        nil];
    NSArray* tabLabels = [NSArray arrayWithObjects:
                          @"Dowód osobisty",
                          @"Dowód rejestracyjny",
                          @"Prawo jazdy",
                          @"Karta miejska",
                          @"Karta Warszawiaka",
                          @"Legitymacja seniora",
                          @"...",
                          @"...",
                          @"...",
                          
                        
                        
                        nil];
    
    NSArray* tabBackgrounds = [NSArray arrayWithObjects:
                          @"karta1",
                          @"karta2",
                          @"karta3",
                          @"karta4",
                          @"karta5",
                          @"karta1",
                          @"karta2",
                          @"karta3",
                          @"karta4",
                          
                          
                          
                          nil];
    
    
    
    
    TabView *tb = [[TabView alloc]init];
    tabArray = [[NSMutableArray alloc]init];
    tb.backgroundColor = [UIColor clearColor];
    [self.backgroundView addSubview:tb];
    
    for (int i = 0; i<8; i++) {
        tb = [[TabView alloc]initWithFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+40+i*45, self.view.frame.size.width, self.view.frame.size.height-45) andColor: [nameArr objectAtIndex:i]];
        tb.backgroundColor = [UIColor clearColor];
        tb.alpha = 1.0;
      
        tb.tabIndex = i;
        //[tb addMotionEffect:group2];
        tb.tabBackground = [UIImage imageNamed:[tabBackgrounds objectAtIndex:i]];
        
        tb.tabLabel.text = [tabLabels objectAtIndex:i];
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
        tapRecognizer.numberOfTapsRequired = 1;
        [tb addGestureRecognizer:tapRecognizer];
        
        
       
        if(i==0){
            UIView *containerView = [[[NSBundle mainBundle] loadNibNamed:@"personalIDViewXIB" owner:self options:nil] lastObject];
            [tb.contentView addSubview:containerView];

        }
        else if (i==1){
            UIView *containerView = [[[NSBundle mainBundle] loadNibNamed:@"carIDXIB" owner:self options:nil] lastObject];
            containerView.frame = CGRectMake(containerView.frame.origin.x, containerView.frame.origin.y-(45*i), containerView.frame.size.width, containerView.frame.size.height);
            [tb.contentView addSubview:containerView];
        }
        else if (i==2){
            UIView *containerView = [[[NSBundle mainBundle] loadNibNamed:@"regInfo" owner:self options:nil] lastObject];
            containerView.frame = CGRectMake(containerView.frame.origin.x, containerView.frame.origin.y-(45*i), containerView.frame.size.width, containerView.frame.size.height);
            [tb.contentView addSubview:containerView];
        }
        
        [tabArray addObject:tb];
        [self.view addSubview:tb];
        
    }
    
    sliderView = [[SliderView alloc]initWithFrame:CGRectMake(0, 283, 320, 570)];
    sliderView.backgroundColor = [UIColor whiteColor];
    
   
    
    
    UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
    
    UIImage *tbImage1 = [UIImage imageNamed:@"1"];
    button1.frame = CGRectMake(32, 32,tbImage1.size.width, tbImage1.size.height);
   
    [button1 setImage:tbImage1 forState:UIControlStateNormal];
    [button1 setTintColor:[UIColor colorWithRed:134/255.0f green:28/255.0f blue:61/255.0f alpha:1.0]];
   
    [button1 addTarget:self
                    action:@selector(button1)
          forControlEvents:UIControlEventTouchUpInside];
    
    [self.sliderView addSubview:button1];
    
    
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    
    UIImage *tbImage2 = [UIImage imageNamed:@"2"];
    button2.frame = CGRectMake(128, 32,tbImage2.size.width, tbImage2.size.height);
    
    [button2 setImage:tbImage2 forState:UIControlStateNormal];
    [button2 setTitleColor:[UIColor colorWithRed:134/255.0f green:28/255.0f blue:61/255.0f alpha:1.0] forState:UIControlStateHighlighted];
    [button2 addTarget:self
               action:@selector(button2)
     forControlEvents:UIControlEventTouchUpInside];
    
    [self.sliderView addSubview:button2];
    
    
    UIButton *button3 = [UIButton buttonWithType:UIButtonTypeCustom];
    
    UIImage *tbImage3 = [UIImage imageNamed:@"3"];
    button3.frame = CGRectMake(224, 32,tbImage3.size.width, tbImage3.size.height);
    
    [button3 setImage:tbImage3 forState:UIControlStateNormal];
    [button3 setTitleColor:[UIColor colorWithRed:134/255.0f green:28/255.0f blue:61/255.0f alpha:1.0] forState:UIControlStateHighlighted];
    [button3 addTarget:self
               action:@selector(button3)
     forControlEvents:UIControlEventTouchUpInside];
    
    [self.sliderView addSubview:button3];
    
    
    UIButton *button4 = [UIButton buttonWithType:UIButtonTypeCustom];
    
    UIImage *tbImage4 = [UIImage imageNamed:@"4"];
    button4.frame = CGRectMake(32, 128,tbImage4.size.width, tbImage4.size.height);
    
    [button4 setImage:tbImage4 forState:UIControlStateNormal];
    [button4 setTitleColor:[UIColor colorWithRed:134/255.0f green:28/255.0f blue:61/255.0f alpha:1.0] forState:UIControlStateHighlighted];
    [button4 addTarget:self
               action:@selector(button4)
     forControlEvents:UIControlEventTouchUpInside];
    
    [self.sliderView addSubview:button4];
    
    
    UIButton *button5 = [UIButton buttonWithType:UIButtonTypeCustom];
    
    UIImage *tbImage5 = [UIImage imageNamed:@"5"];
    button5.frame = CGRectMake(128, 128,tbImage5.size.width, tbImage5.size.height);
    
    [button5 setImage:tbImage5 forState:UIControlStateNormal];
    [button5 setTitleColor:[UIColor colorWithRed:134/255.0f green:28/255.0f blue:61/255.0f alpha:1.0] forState:UIControlStateHighlighted];
    [button5 addTarget:self
               action:@selector(button5)
     forControlEvents:UIControlEventTouchUpInside];
    
    [self.sliderView addSubview:button5];
    
    
    UIButton *button6 = [UIButton buttonWithType:UIButtonTypeCustom];
    
    UIImage *tbImage6 = [UIImage imageNamed:@"6"];
    button6.frame = CGRectMake(224, 128,tbImage6.size.width, tbImage6.size.height);
    
    [button6 setImage:tbImage6 forState:UIControlStateNormal];
    [button6 setTitleColor:[UIColor colorWithRed:134/255.0f green:28/255.0f blue:61/255.0f alpha:1.0] forState:UIControlStateHighlighted];
    [button6 addTarget:self
               action:@selector(button6)
     forControlEvents:UIControlEventTouchUpInside];
    
    [self.sliderView addSubview:button6];
    

    
    
    
    
   buttonLeft = [UIButton buttonWithType:UIButtonTypeCustom];
    
    
    buttonLeft.frame = CGRectMake(-40, 240, 200, 30);
    [buttonLeft setTitle:@"Zarządzaj" forState:UIControlStateNormal];
    buttonLeft.titleLabel.textColor = [UIColor blackColor];

    [buttonLeft setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [buttonLeft setTitleColor:[UIColor colorWithRed:134/255.0f green:28/255.0f blue:61/255.0f alpha:1.0] forState:UIControlStateHighlighted];
    [buttonLeft setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
    
    [buttonLeft addTarget:self
                action:@selector(buttonLeftClick)
      forControlEvents:UIControlEventTouchUpInside];
    
    [self.sliderView addSubview:buttonLeft];
    
    
    
    //134 28 61
    
    buttonRight = [UIButton buttonWithType:UIButtonTypeCustom];
    
    
    buttonRight.frame = CGRectMake(155, 240, 200, 30);
    [buttonRight setTitle:@"Ustawienia" forState:UIControlStateNormal];
    buttonRight.titleLabel.textColor = [UIColor blackColor];
    [buttonRight setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [buttonRight setTitleColor:[UIColor colorWithRed:134/255.0f green:28/255.0f blue:61/255.0f alpha:1.0] forState:UIControlStateHighlighted];
    [buttonRight setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
    
    [buttonRight addTarget:self
                   action:@selector(buttonRightClick)
         forControlEvents:UIControlEventTouchUpInside];
    
    [self.sliderView addSubview:buttonRight];
    
    
   
    
    
    
    
    
    [self.view addSubview:sliderView];
    [self.view addSubview:_titleLabel];
    
}

-(void)button1{


}

-(void)button2{
    
    
}
-(void)button3{
    
    
}
-(void)button4{
    
    
}
-(void)button5{
    
    
}
-(void)button6{
    
    
}
-(void)buttonLeftClick{
  
}
-(void)buttonRightClick{
   
    
}


- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark Parse Login Delegate
// Sent to the delegate to determine whether the log in request should be submitted to the server.
- (BOOL)logInViewController:(PFLogInViewController *)logInController shouldBeginLogInWithUsername:(NSString *)username password:(NSString *)password {
    // Check if both fields are completed
    if (username && password && username.length != 0 && password.length != 0) {
        return YES; // Begin login process
    }
    
    [[[UIAlertView alloc] initWithTitle:@"Missing Information"
                                message:@"Make sure you fill out all of the information!"
                               delegate:nil
                      cancelButtonTitle:@"ok"
                      otherButtonTitles:nil] show];
    return NO; // Interrupt login process
}

// Sent to the delegate when a PFUser is logged in.
- (void)logInViewController:(PFLogInViewController *)logInController didLogInUser:(PFUser *)user {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

// Sent to the delegate when the log in attempt fails.
- (void)logInViewController:(PFLogInViewController *)logInController didFailToLogInWithError:(NSError *)error {
    NSLog(@"Failed to log in...");
}

// Sent to the delegate when the log in screen is dismissed.
- (void)logInViewControllerDidCancelLogIn:(PFLogInViewController *)logInController {
    [self.navigationController popViewControllerAnimated:YES];
}

// Sent to the delegate to determine whether the sign up request should be submitted to the server.
- (BOOL)signUpViewController:(PFSignUpViewController *)signUpController shouldBeginSignUp:(NSDictionary *)info {
    BOOL informationComplete = YES;
    
    // loop through all of the submitted data
    for (id key in info) {
        NSString *field = [info objectForKey:key];
        if (!field || field.length == 0) { // check completion
            informationComplete = NO;
            break;
        }
    }
    
    // Display an alert if a field wasn't completed
    if (!informationComplete) {
        [[[UIAlertView alloc] initWithTitle:@"Missing Information"
                                    message:@"Make sure you fill out all of the information!"
                                   delegate:nil
                          cancelButtonTitle:@"ok"
                          otherButtonTitles:nil] show];
    }
    
    return informationComplete;
}

// Sent to the delegate when a PFUser is signed up.
- (void)signUpViewController:(PFSignUpViewController *)signUpController didSignUpUser:(PFUser *)user {
    [self dismissModalViewControllerAnimated:YES]; // Dismiss the PFSignUpViewController
}

// Sent to the delegate when the sign up attempt fails.
- (void)signUpViewController:(PFSignUpViewController *)signUpController didFailToSignUpWithError:(NSError *)error {
    NSLog(@"Failed to sign up...");
}

// Sent to the delegate when the sign up screen is dismissed.
- (void)signUpViewControllerDidCancelSignUp:(PFSignUpViewController *)signUpController {
    NSLog(@"User dismissed the signUpViewController");
}


BOOL tabClicked = NO;
-(void)tap:(UITapGestureRecognizer*)tapRecognizer{
    
    
    TabView *myTabView = tapRecognizer.view;
    
    NSLog(@"fire %i", myTabView.tabIndex);
    
    if (myTabView.clicked ==NO){
        [UIView animateWithDuration:0.33f
                              delay:0.0f
                            options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionAllowUserInteraction
                         animations:^{
                             if(myTabView.tabIndex == 0){
                                 myTabView.frame = CGRectMake(-15, 40, 350, 528);
                                 
                             }
                             else{
                                 myTabView.frame = CGRectMake(-15, 40, 350, 528);
                             }
                             
                             sliderView.frame = CGRectMake(0, 568, sliderView.bounds.size.width, sliderView.bounds.size.height);
                            
                             for (int i=myTabView.tabIndex+1; i<tabArray.count;i++){
                                 
                                 ((TabView*)[tabArray objectAtIndex:i]).frame = CGRectMake(0, 568, ((TabView*)[tabArray objectAtIndex:i]).bounds.size.width, ((TabView*)[tabArray objectAtIndex:i]).bounds.size.height);
                                 
                                 
                                 
                             }
                         }
                         completion:^(BOOL finished){
                             myTabView.clicked = YES;
                             
                             
                             
                             [UIView animateWithDuration:0.33f
                                                   delay:0.0f
                                                 options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionAllowUserInteraction
                                              animations:^{
                                               myTabView.tabButton.alpha = 1.0;
                                                  
                                              }
                                              completion:^(BOOL finished){
                                                  myTabView.clicked = YES;
                                                  
                                                  
                                                  
                                                  
                                                  
                                                  
                                                  
                                              }];
                             
                             
                             
                         }];
    }
    if (myTabView.clicked ==YES){
        [UIView animateWithDuration:0.1f
                              delay:0.0f
                            options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionAllowUserInteraction
                         animations:^{
                             
                              myTabView.tabButton.alpha = 0.0;
                         }
                         completion:^(BOOL finished){
                             myTabView.clicked = NO;
                             
                             
                             
                             
                             
                             
                             
                             [UIView animateWithDuration:0.33f
                                                   delay:0.0f
                                                 options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionAllowUserInteraction
                                              animations:^{
                                                  myTabView.frame = CGRectMake(0, myTabView.tabIndex*45+40, 320, myTabView.bounds.size.height);
                                                  sliderView.frame = CGRectMake(0, 283, sliderView.bounds.size.width, sliderView.bounds.size.height);
                                                  
                                                  for (int i=myTabView.tabIndex+1; i<tabArray.count;i++){
                                                      
                                                      ((TabView*)[tabArray objectAtIndex:i]).frame = CGRectMake(0, ((TabView*)[tabArray objectAtIndex:i]).tabIndex*45+40, ((TabView*)[tabArray objectAtIndex:i]).bounds.size.width, ((TabView*)[tabArray objectAtIndex:i]).bounds.size.height);
                                                  }
                                                  
                                              }
                                              completion:^(BOOL finished){
                                                  
                                              }];}];
        
        
    }
}

#pragma mark data approve
-(void) approveData{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Dostęp do danych medycznych"
                                                    message:@"Dr. Michał Stasiuk prosi o dostęp do danych medycznych"
                                                   delegate:self
                                          cancelButtonTitle:@"Zezwól"
                                          otherButtonTitles:@"Zabroń", nil];
    [alert show];
}


@end
