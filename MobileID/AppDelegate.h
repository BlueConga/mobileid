//
//  AppDelegate.h
//  MobileID
//
//  Created by Filip Jakubowski on 15.03.2014.
//  Copyright (c) 2014 HEMNES. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
