//
//  main.m
//  MobileID
//
//  Created by Filip Jakubowski on 15.03.2014.
//  Copyright (c) 2014 HEMNES. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
