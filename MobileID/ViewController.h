//
//  ViewController.h
//  MobileID
//
//  Created by Filip Jakubowski on 15.03.2014.
//  Copyright (c) 2014 HEMNES. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface ViewController : UIViewController <PFLogInViewControllerDelegate, PFSignUpViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
-(void) approveData;
-(void) slide;
@end
